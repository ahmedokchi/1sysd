#include<stdio.h>
#include<stdlib.h>


int main(int argc, char *argv[]) {
    // argc = argument count ; combien d'argument j'ai reçu
    // argv = arguments values ; quels sont-ils ?
    // argv[0] : nom du programme exécuté
    // argv[1] : premier argument,
    // argv[2] : deuxième argument, etc.
    double var = 0;
    for (int i = 1; i < argc; i++) {
        *argv[i] = strtod(argv[i], NULL);
        var += *argv[i];
    }
    printf("%f\n", var);

    exit(0);

}

