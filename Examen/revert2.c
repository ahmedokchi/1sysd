#include<stdio.h>
#include<stdlib.h>

char *reverse_str(char *s) {
    char *p, *q;
    char *rs; 
    int len = 0; 

    p = s;
    while (*p++) {
        len++;
    }
    rs = malloc((len + 1)*sizeof(char));
    q = rs;
    for (int i = len - 1; i >= 0; i--) {
        *q++ = s[i];
    }
    *q = '\0';
    return rs;
}
int main(int argc, char *argv[]) {

    for(int i = argc-1; i > 0; i--) {
        printf("%s\n", reverse_str(argv[i]));
    } 
    }

