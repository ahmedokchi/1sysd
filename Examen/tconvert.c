#include<stdio.h>
#include<stdlib.h>



int celsius2fahrenheit(float *f) {
    float F;
    F = (9/5) * *f + 32;
    return F;
}

int fahrenheit2celsius(float *f) {
    float C;
    C = (*f - 32)*5/9;
    return C;
}


int main() {

    int type;
    float valeur, conversion;

    printf("Veuillez écrire la valeur que vous souhaitez convertir.\n");
    scanf("%f", &valeur);
    printf("Si vous souhaitez passez de °C à °F écrivez 1, si c'est le cas contraire écrivez 2.\n");
    scanf("%d", &type);

    if (type == 1) {
        celsius2fahrenheit(&valeur);
        conversion = celsius2fahrenheit(&valeur);
        printf("%f °C est %f en °F\n", valeur, conversion);
    } else if (type == 2) {
        fahrenheit2celsius(&valeur);
        conversion = fahrenheit2celsius(&valeur);
        printf("%f °F est en %f °C\n", valeur, conversion);
    } else {
        printf("ERREUR\n");
    }




    exit(0);
}

