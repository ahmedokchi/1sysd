#include<stdio.h>
#include<stdlib.h>

int count_char(char *caractere, char *chaine) {
    int count = 0;
    while (*chaine != '\0') {
        if (*chaine == *caractere) {
            count += 1;
        }
        chaine +=1;
    }
    return count;
}

int main(int argc, char *argv[]) {
    char *caractere = argv[1];
    char *chaine = argv[2]; 
    int n = count_char(caractere, chaine);
    printf("Le caractere %s est présent %d fois dans la chaine.\n", caractere, n);

}